from capstone.models import *
from capstone.serializers import *
from random import randint

def fight(attacker, defender, skill):
    result = {'player': '', 'monster': ''}
    attackroll = randint(1, 20)

    if skill.skillType == 1:
        # Single target damage
        countDownStatus(attacker, defender)
        if attackroll == 20:
            rawdamage = (skill.power + getBonusDamage(attacker.strength)) * getCritMultiplier(attacker.dexterity)
            damagereduced = rawdamage * getReductions(defender.armor)
            defender.hitPoints -= int(rawdamage - damagereduced)
            result['player'] = 'crit ' + str(rawdamage - damagereduced)
        elif attackroll > 1:
            rawdamage = (skill.power + getBonusDamage(attacker.strength))
            damagereduced = rawdamage * getReductions(defender.armor)
            defender.hitPoints -= int(rawdamage - damagereduced)
            result['player'] = 'hit ' + str(rawdamage - damagereduced)
        else:
            result['player'] = 'miss 0'

    elif skill.skillType == 2:
        # Heal
        countDownStatus(attacker, defender)
        if attackroll == 20:
            attacker.hitPoints += (skill.power * 2) * getHealMultiplier(attacker.wisdom)
            result['player'] = 'critheal ' + str((skill.power * 2) * getHealMultiplier(attacker.wisdom))
        elif attackroll > 1:
            attacker.hitPoints += skill.power * getHealMultiplier(attacker.wisdom)
            result['player'] = 'heal ' + str(skill.power * getHealMultiplier(attacker.wisdom))
        else:
            result['player'] = 'failedheal'
        if attacker.hitPoints > 100 + (10 * attacker.level):
            attacker.hitPoints = 100 + (10 * attacker.level)

    elif skill.skillType == 3:
        # Buff
        if attackroll > 1:
            result['player'] = 'buffsuccess'
            applyEffects(attacker, skill)
            statusEntry = {'characterID': attacker.characterID, 'skillID': skill.skillID, 'turnsRemaining': skill.duration + getEffectMultiplier(attacker.wisdom)}
            status = StatusSerializer(data=statusEntry)
            if status.is_valid():
                status.save()
            else:
                print(status.errors)
        else:
            result['player'] = 'failedeffect'

    else:
        # Debuff
        if attackroll > 1:
            result['player'] = 'debuffsuccess'
            applyEffects(defender, skill)
            statusEntry = {'characterID': defender.characterID, 'skillID': skill.skillID, 'turnsRemaining': skill.duration + getEffectMultiplier(attacker.wisdom)}
            status = StatusSerializer(data=statusEntry)
            if status.is_valid():
                status.save()
            else:
                print(status.errors)
        else:
            result['player'] = 'failedeffect'
    
    attackroll = randint(20, 20 + defender.level)
    rawdamage = (attackroll + getBonusDamage(defender.strength))
    damagereduced = rawdamage * getReductions(attacker.armor)
    attacker.hitPoints -= int(rawdamage - damagereduced)
    result['monster'] = str(rawdamage - damagereduced)
    resultString = getResult(result)

    return resultString
    

def getReductions(armor):
    reduction = armor / (armor + 100)
    if reduction > .75:
        reduction = .75

    return reduction

def getCritMultiplier(dexterity):
    return 1.2 + (.05 * dexterity)

def getBonusDamage(strength):
    try:
        return randint(strength // 4, strength // 2)
    except:
        return 0

def getEffectMultiplier(wisdom):
    return wisdom // 5

def getHealMultiplier(wisdom):
    return 1.2 + (.1 * wisdom)

def applyEffects(character, skill):
    character.armor += skill.armor
    character.strength += skill.strength
    character.dexterity += skill.dexterity
    character.wisdom += skill.wisdom

def removeEffects(character, skill):
    character.armor -= skill.armor
    character.strength -= skill.strength
    character.dexterity -= skill.dexterity
    character.wisdom -= skill.wisdom

def countDownStatus(attacker, defender):
    statuses = Status.statuses.all()
    attackerStatuses = []
    defenderStatuses = []
    for status in statuses:
        if status.characterID == attacker.characterID:
            status.turnsRemaining -= 1
            if status.turnsRemaining <= 0:
                skillToRemove = Skills.skills.get(skillID = status.skillID)
                removeEffects(attacker, skillToRemove)
                status.delete()
            else:
                status.save()
        elif status.characterID == defender.characterID:
            status.turnsRemaining -= 1
            if status.turnsRemaining <= 0:
                skillToRemove = Skills.skills.get(skillID = status.skillID)
                removeEffects(defender, skillToRemove)
                status.delete()
            else:
                status.save()

def getResult(fightValues):
    # Formatted fight string
    if 'crit ' in fightValues['player']:
        return "You attacked the monster and hit for " + fightValues['player'].split()[1].split('.')[0] + ". Critical! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'hit' in fightValues['player']:         
        return "You attacked the monster and hit for " + fightValues['player'].split()[1].split('.')[0] + ". The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'miss' in fightValues['player']:
        return "You attacked the monster and missed! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'critheal' in fightValues['player']:   
        return "You healed yourself for " + fightValues['player'].split()[1].split('.')[0] + ". Critical! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'heal ' in fightValues['player']:  
        return "You healed yourself for " + fightValues['player'].split()[1].split('.')[0] + ". The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'failedheal' in fightValues['player']:
        return "Your heal failed! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'buffsuccess' in fightValues['player']:
        return "Your spell was successful! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'debuffsuccess' in fightValues['player']:
        return "Your curse was successful! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    elif 'failedeffect' in fightValues['player']:
        return "Your spell failed! The monster hit you for " + fightValues['monster'].split('.')[0]  + "!"
    else:
        return "Fight parse error"
