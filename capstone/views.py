import re
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from capstone.models import *
from capstone.serializers import *
import json
import capstone.combat
from capstone import combat
# from django.shortcuts import render

# Create your views here.
@csrf_exempt
def getReward(request):
    """
    Retrieve reward upon completion of an event
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            player = Character.characters.get(characterID = data['characterID'])
        except Character.DoesNotExist:
            return HttpResponse("Character does not exist!")
        try:
            event = Event.events.get(eventID = data['eventID'])
        except Event.DoesNotExist:
            return HttpResponse("Event does not exist!")

        inventories = Inventory.inventories.all()
        hasItem = False
        for i in inventories:
            if str(i.characterID) == data['characterID'] and i.itemID == event.reward:
                hasItem = True
                i.count += 1
                i.save()

        if not hasItem:
            # Player doesn't have this item yet
            inventorydict = {'characterID': data['characterID'], 'itemID': event.reward, 'count': 1, 'isEquipped': 1}
            inventory = InventorySerializer(data=inventorydict)
            if inventory.is_valid():
                inventory.save()
                # Remove stat updates once equipping items is working
                item = Item.items.get(itemID = event.reward)
                player.armor += item.armor
                player.strength += item.strength
                player.dexterity += item.dexterity
                player.wisdom += item.wisdom
                player.save()
            else:
                print(serializer.errors)
                return HttpResponse("\nError adding item to character inventory")

        newInventories = []
        for inv in inventories:
            if inv.characterID == data['characterID']:
                newInventories.append(inv)
        serializer = InventorySerializer(newInventories, many=True)
        
        return JsonResponse(serializer.data, safe=False, status=201)

@csrf_exempt
def equip(request):
    """
    Equip or unequip an item
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        inventories = Inventory.inventories.all()
        characterItems = []
        for inv in inventories:
            if inv.characterID == data['characterID']:
                player = Character.characters.get(characterID = data['characterID'])
                item = Item.items.get(itemID = data['itemID'])
                if inv.itemID == data['itemID']:
                    inv.isEquipped = 1
                    player.armor += item.armor
                    player.strength += item.strength
                    player.dexterity += item.dexterity
                    player.wisdom += item.wisdom
                    player.save()
                else:
                    inv.isEquipped = 0
                    player.armor -= item.armor
                    player.strength -= item.strength
                    player.dexterity -= item.dexterity
                    player.wisdom -= item.wisdom
                    player.save()
                inv.save()


@csrf_exempt
def chat(request):
    """
    Post a chat message or retrieve the last 20 messages
    """   
    if request.method == 'POST':
        data = JSONParser().parse(request)
        unformattedTime = data['messageTime']
        m = re.findall(r'(?<=T)\d+:\d+', unformattedTime)
        serializer = MessageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(m[0], safe=False, status=201)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'GET':
        recentMessages = Message.messages.filter().order_by('-messageID')[:20][::-1]
        for mess in recentMessages:
            m = re.findall(r'(?<=\s)\d+:\d+', str(mess.messageTime))
            mess.messageTime = m[0]
        serializer = MessageSerializer(recentMessages, many=True)

        return JsonResponse(serializer.data, safe=False, status=201)

@csrf_exempt
def login(request):
    """
    Login
    """
    data = JSONParser().parse(request)
    try:
        user = User.users.get(email = data["email"])
    except User.DoesNotExist:
        return HttpResponse("Invalid Username")
    if user.password == data["password"]:
        character = Character.characters.get(characterID = user.characterID)
        serializer = CharacterSerializer(character)
        return JsonResponse(serializer.data, safe=False)
    else:
        return HttpResponse("Invalid Password")

@csrf_exempt
def signup(request):
    """
    Signup
    """
    data = JSONParser().parse(request)
    lastID = Character.characters.latest("characterID").characterID
    print(lastID)
    characterdict = {'characterID': lastID + 1, 'name': data["character_name"], 'hitPoints': 100, 'armor': 50, 'strength': 10, 'dexterity': 10, 'wisdom': 10, 'level': 1}
    inventorydict = {'characterID': lastID + 1, 'itemID': 4, 'count': 1, 'isEquipped': 1}
    inventory = InventorySerializer(data=inventorydict)
    if inventory.is_valid():
        inventory.save()
    else:
        print(serializer.errors)
        return HttpResponse("\nError creating character inventory")
    serializer = CharacterSerializer(data=characterdict)
    if serializer.is_valid():
        serializer.save()
    else:
        print(serializer.errors)
        return HttpResponse("\nInvalid Character")
    character = Character.characters.latest("characterID").characterID
    userdict = {"email": data["email"], "password": data["password"], "characterID": character}
    userserializer = UserSerializer(data=userdict)
    if userserializer.is_valid():
        userserializer.save()
    else:
        print(userserializer.errors)
        return HttpResponse("\nInvalid User")
    
    return JsonResponse(serializer.data, status=201)

@csrf_exempt
def fight(request):
    data = JSONParser().parse(request)
    #for debug
    print(data)
    try:
        player = Character.characters.get(characterID = data["attackerID"])
    except Character.DoesNotExist:
        return HttpResponse("Attacker not found")
    try:
        monster = Character.characters.get(characterID = data["defenderID"])
    except Character.DoesNotExist:
        return HttpResponse("Defender not found")
    try:
        skill = Skills.skills.get(skillID = data["skillID"])
    except Skills.DoesNotExist:
        return HttpResponse("Skill not found")
    fightResult = combat.fight(player, monster, skill)
    playerdict = {'characterID': player.characterID, 'name': player.name, 'hitPoints': player.hitPoints, 'armor': player.armor, 'strength': player.strength, 'dexterity': player.dexterity, 'wisdom': player.wisdom, 'level': player.level}
    monsterdict = {'characterID': monster.characterID, 'name': monster.name, 'hitPoints': monster.hitPoints, 'armor': monster.armor, 'strength': monster.strength, 'dexterity': monster.dexterity, 'wisdom': monster.wisdom, 'level': monster.level}
	
    response = {"player": playerdict, "monster": monsterdict, "result": fightResult}    
    
    if player.hitPoints <= 0:
        player.hitPoints = 100 + (player.level * 10)
        monster.hitPoints += int((100 + (monster.level * 10)) * .5)
        if monster.hitPoints > 100 + (monster.level * 10):
            monster.hitPoints = 100 + (monster.level * 10)
    if monster.hitPoints <= 0:
        monster.hitPoints = 100 + (monster.level * 10)
        player.hitPoints = 100 + (player.level * 10)
 
    player.save()
    monster.save()

    return JsonResponse(response, status=201)

@csrf_exempt
def character_list(request):
    """
    List all characters, or create a new character.
    """
    if request.method == 'GET':
        characters = Character.characters.all()
        serializer = CharacterSerializer(characters, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CharacterSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def character_detail(request, pk):
    """
    Retrieve, update or delete a character.
    """
    try:
        character = Character.characters.get(pk=pk)
    except Character.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CharacterSerializer(character)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CharacterSerializer(character, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        character.delete()
        return HttpResponse(status=204)

@csrf_exempt
def item_list(request):
    """
    Retrieve all items, or add a new item.
    """
    if request.method == 'GET':
        items = Item.items.all()
        serializer = ItemSerializer(items, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ItemSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def event_list(request):
    """
    Retrieve all events. Event creation will be handled by the server, so post is unnecessary.
    """
    if request.method == 'GET':
        events = Event.events.all()
        serializer = EventSerializer(events, many=True)
        return JsonResponse(serializer.data, safe=False)

@csrf_exempt
def nearby_events(request):
    """
    Retrieve only nearby events
    """
    lat = request.GET['lat']
    lon = request.GET['lon']
    # lower map threshold to restrict the nearby events that can be seen
    MAP_THRESHOLD = 5
    if request.method == 'GET':
        events = list(Event.events.all())
        for ev in events:
            if abs(float(lat) - float(ev.latitude)) > MAP_THRESHOLD or abs(float(lon) - float(ev.longitude)) > MAP_THRESHOLD:
                events.remove(ev)
    serializer = EventSerializer(events, many=True)
    return JsonResponse(serializer.data, safe=False)

@csrf_exempt
def event_detail(request, pk):
    """
    Retrieve a specific event
    """
    try:
        event = Event.events.get(pk=pk)
    except Event.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = EventSerializer(event)
        return JsonResponse(serializer.data)

    elif request.method == 'DELETE':
        event.delete()
        return HttpResponse(status=204)

@csrf_exempt
def player_inventory(request, characterID):
    """
    Retrieve the items in a player's inventory
    """
    if request.method == 'GET':
        characterInventory = Inventory.inventories.all()
        inventories = []
        for inv in characterInventory:
            if str(inv.characterID) == str(characterID):
                serializer = InventorySerializer(inv)
                inventories.append(serializer.data)
        if len(inventories) == 0:
            return HttpResponse(status=403)

        itemList = []
        for i in inventories:
            try:
                item = Item.items.get(itemID = i["itemID"])
            except Item.DoesNotExist:
                continue
            serializer = ItemSerializer(item)
        
            itemList.append(serializer.data)
        return JsonResponse(itemList, safe=False)

@csrf_exempt
def event_monsters(request, eventID):
    """
    Retrieve a list of monsters in the specified event lobby
    """
    if request.method == 'GET':
        allEvents = EventMonsters.eventMonsters.all()
        eventEntries = []
        for e in allEvents:
            if str(e.eventID) == str(eventID):
                serializer = EventMonstersSerializer(e)
                eventEntries.append(serializer.data)
        if len(eventEntries) == 0:
            return HttpResponse(status=404)

        monsters = []
        for ev in eventEntries:
            try:
                character = Character.characters.get(characterID = ev["characterID"])
            except Character.DoesNotExist:
                continue
            serializer = CharacterSerializer(character)

            monsters.append(serializer.data)        
        return JsonResponse(monsters, safe=False)

@csrf_exempt
def skill_detail(request, skillID):
    """
    Retrieves skill data
    """
    if request.method == 'GET':
        try:
            skill = Skills.skills.get(skillID = skillID)
            serializer = SkillSerializer(skill)
            return JsonResponse(serializer.data)

        except Skills.DoesNotExist:
            return HttpResponse(status=404)

@csrf_exempt
def item_skills(request, itemID):
    """
    Retrieves the skills belonging to an item
    """
    if request.method == 'GET':
        items = ItemSkills.itemSkills.all()
        itemEntries = []
        for i in items:
            if str(i.itemID) == str(itemID):
                serializer = ItemSkillsSerializer(i)
                itemEntries.append(serializer.data)
        if len(itemEntries) == 0:
            return HttpResponse(status=404)
        
        skillslist = []
        for item in itemEntries:
            try:
                skill = Skills.skills.get(skillID = item["skillID"])
            except Skills.DoesNotExist:
                continue
            serializer = SkillSerializer(skill)

            skillslist.append(serializer.data)
        return JsonResponse(skillslist, safe=False)

@csrf_exempt
def character_skills(request, characterID):
    """
    Retrieves skills for a player
    """
    if request.method == 'GET':
        characterInventory = Inventory.inventories.all()
        inventories = []
        for inv in characterInventory:
            if inv.characterID == characterID and inv.isEquipped == 1:
                serializer = InventorySerializer(inv)
                inventories.append(serializer.data)
        if len(inventories) == 0:
            return HttpResponse(status=404)

        itemList = []
        for i in inventories:
            try:
                item = Item.items.get(itemID = i["itemID"])
            except Item.DoesNotExist:
                continue
            
            itemList.append(item.itemID)
        
        items = ItemSkills.itemSkills.all()
        itemEntries = []
        for il in itemList:
            for i in items:
                if str(i.itemID) == str(il):
                    serializer = ItemSkillsSerializer(i)
                    itemEntries.append(serializer.data)
        if len(itemEntries) == 0:
            return HttpResponse(status=404)

        skillslist = []
        for item in itemEntries:
            try:
                skill = Skills.skills.get(skillID = item["skillID"])
            except Skills.DoesNotExist:
                continue
            serializer = SkillSerializer(skill)

            skillslist.append(serializer.data)
        return JsonResponse(skillslist, safe=False) 
