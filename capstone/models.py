# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    last_name = models.CharField(max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    action_flag = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'

# class CharacterManager(models.Manager):
#   def create(self, kwargs):
#       character = self.create(**kwargs)

#       return character

# class ItemManager(models.Manager):
#   def create(self, kwargs):
#       item = self.create(kwargs)

#       return item

# class EventManager(models.Manager):
#   def create(self, kwargs):
#       event = self.create(kwargs)

#       return event

class User(models.Model):
    email = models.CharField(primary_key = True, max_length = 254)
    password = models.CharField(max_length = 128)
    characterID = models.IntegerField();

    users = models.Manager()    

    class Meta:
        managed = True
        db_table = "Users"

class Character(models.Model):
    characterID = models.IntegerField(primary_key = True)
    name = models.CharField(max_length = 30)
    hitPoints = models.PositiveIntegerField()
    armor = models.IntegerField()
    strength = models.IntegerField()
    dexterity = models.IntegerField()
    wisdom = models.IntegerField()
    level = models.PositiveIntegerField()

    characters = models.Manager()

    class Meta:
        managed = True
        db_table = "Characters"

class Message(models.Model):
    messageID = models.IntegerField(primary_key = True)
    sender = models.CharField(max_length = 30)
    messageText = models.CharField(max_length = 255)
    messageTime = models.DateTimeField(auto_now_add = True)

    messages = models.Manager()
    
    class Meta:
        managed = True
        db_table = "Messages"

class Event(models.Model):
    eventID = models.IntegerField(primary_key = True)
    latitude = models.DecimalField(decimal_places = 10, max_digits = 20)
    longitude = models.DecimalField(decimal_places = 10, max_digits = 20)
    eventType = models.IntegerField()
    name = models.CharField(max_length = 80)
    description = models.TextField()
    reward = models.IntegerField()

    events = models.Manager()

    class Meta:
        managed = True
        db_table = "Events"

class Item(models.Model):
    itemID = models.IntegerField(primary_key = True)
    itemType = models.IntegerField()
    name = models.CharField(max_length = 80)
    description = models.TextField()
    isSellable = models.BooleanField()
    equipType = models.IntegerField()
    hitPoints = models.IntegerField()
    armor = models.IntegerField()
    strength = models.IntegerField()
    dexterity = models.IntegerField()
    wisdom = models.IntegerField()
    numberOfUses = models.PositiveIntegerField()

    items = models.Manager()

    class Meta:
        managed = True
        db_table = "Items"

class Inventory(models.Model):
    characterID = models.IntegerField()
    itemID = models.IntegerField()
    count = models.PositiveIntegerField()
    isEquipped = models.BooleanField()

    inventories = models.Manager()

    class Meta:
        managed = True
        db_table = "Inventory"

class Status(models.Model):
    characterID = models.IntegerField()
    skillID = models.IntegerField()
    turnsRemaining = models.IntegerField()

    statuses = models.Manager()

    class Meta:
        managed = True
        db_table = "StatusEffects"

class Skills(models.Model):
    skillID = models.IntegerField(primary_key = True)
    name = models.CharField(max_length = 30)
    skillType = models.IntegerField()
    power = models.PositiveIntegerField()
    duration = models.PositiveIntegerField()
    cooldown = models.PositiveIntegerField()
    armor = models.IntegerField()
    strength = models.IntegerField()
    dexterity = models.IntegerField()
    wisdom = models.IntegerField()

    skills = models.Manager()

    class Meta:
        managed = True
        db_table = "Skills"

class ItemSkills(models.Model):
    skillID = models.IntegerField()
    itemID = models.IntegerField()

    itemSkills = models.Manager()

    class Meta:
        managed = True
        db_table = "ItemSkills"


class EventMonsters(models.Model):
    eventID = models.IntegerField()
    characterID = models.IntegerField()

    eventMonsters = models.Manager()

    class Meta:
        managed = True
        db_table = "EventMonsters"
