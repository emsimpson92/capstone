from enum import Enum

class EquipType(Enum):
    WEAPON = 1
    ARMOR = 2
    SKILL = 3
