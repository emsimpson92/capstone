from enum import Enum

class EventType(Enum):
    PVP = 1
    QUEST = 2
    ENCOUNTER = 3
