from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Character)
admin.site.register(Event)
admin.site.register(Item)
admin.site.register(Inventory)
admin.site.register(ItemSkills)
admin.site.register(EventMonsters)
admin.site.register(User)
admin.site.register(Message)
admin.site.register(Skills)
