from rest_framework import serializers
from capstone.models import *


class CharacterSerializer(serializers.ModelSerializer):

	def create(self, validated_data):
		return Character.characters.create(**validated_data)

	class Meta:
		model = Character
		fields = ['characterID',
        'name',
        'hitPoints',
        'armor',
        'strength',
        'dexterity',
        'wisdom',
        'level',
        ]

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email',
        'password',
        'characterID',
        ]

class StatusSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Status
        fields = ['characterID',
        'skillID',
        'turnsRemaining',
        ]

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['sender',
        'messageText',
        'messageTime',
        ]

class ItemSerializer(serializers.ModelSerializer):
	class Meta:
		model = Item
		fields = ['itemType',
		'name',
		'description',
		'isSellable',
		'equipType',
		'hitPoints',
		'armor',
		'strength',
		'dexterity',
		'wisdom',
		'numberOfUses',
		]

class EventSerializer(serializers.ModelSerializer):
	class Meta:
		model = Event
		fields = ['eventID',
        'latitude',
		'longitude',
		'eventType',
		'name',
		'description',
		'reward',
		]

class InventorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Inventory
		fields = ['characterID',
		'itemID',
		'count',
        'isEquipped',
		]

class SkillSerializer(serializers.ModelSerializer):
	class Meta:
		model = Skills
		fields = ['name',
		'skillID',
		'skillType',
		'power',
		'duration',
		'cooldown',
		'armor',
		'strength',
		'dexterity',
		'wisdom',
		]

class ItemSkillsSerializer(serializers.ModelSerializer):
	class Meta:
		model = ItemSkills
		fields = ['skillID',
		'itemID',
		]

class EventMonstersSerializer(serializers.ModelSerializer):
	class Meta:
		model = EventMonsters
		fields = ['eventID',
		'characterID',
		]
        
# class CharacterSerializer(serializers.Serializer):
#     name = serializers.CharField(required=True)
#     password = serializers.CharField()
#     HP = serializers.IntegerField()
#     armor = serializers.IntegerField()
#     strength = serializers.IntegerField()
#     dexterity = serializers.IntegerField()
#     wisdom = serializers.IntegerField()
#     level = serializers.IntegerField()

#     def create(self, validated_data):
#         """
#         Create and return a new `Character` instance, given
#         the validated data.
#         """
#         return Character.objects.create(**validated_data)

#     def update(self, instance, validated_data):
#         """
#         Update and return an existing `Character` instance
#         given the validated data
#         """
#         instance.name = validated_data.get('name', instance.title)
#         instance.password = validated_data.get('password', instance.password)
#         instance.HP = validated_data.get('HP', instance.HP)
#         instance.armor = validated_data.get('armor', instance.armor)
#         instance.dexterity = validated_data.get('dexterity', instance.dexterity)
#         instance.wisdom = validated_data.get('wisdom', instance.wisdom)
#         instance.level = validated_data.get('level', instance.level)
#         instance.save()
#         return instance
