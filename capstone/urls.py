from django.urls import path
from capstone import views

urlpatterns = [
    # Get reward for an event
    path('getReward/', views.getReward),

    # Equip or unequip an item
    path('equip/', views.equip),
    
    # Post a chat message or retrieve latest 20 messages
    path('chat/', views.chat),

    # Receives and returns an attack to the player
    path('fight/', views.fight),

    # Login
    path('login/', views.login),

    # Signup
    path('signup/', views.signup),

    # List all characters
    path('characters/', views.character_list),
    
    # List specific character. pk = primary key, AKA characterID
    path('characters/<str:pk>/', views.character_detail),
    
    # List all items
    path('items/', views.item_list),
    
    # List all events
    path('allevents/', views.event_list),
    
    # List specific event. pk = primary key, AKA eventID
    path('events/<str:pk>/', views.event_detail),
    
    # List nearby events
    # events?lat={latitudevalue}&lon={longitudevalue}
    path('events/', views.nearby_events),

    # List all items in specific character's inventory
    path('inventory/<str:characterID>/', views.player_inventory),
    
    # List all monsters linked to specific event
    path('eventmonsters/<str:eventID>/', views.event_monsters),
    
    # List specific skill
    path('skills/<str:skillID>/', views.skill_detail),
    
    # List all skills linked to specific item
    path('itemskills/<str:itemID>/', views.item_skills),

    # List all skills available to specific player
    path('characterskills/<str:characterID>/', views.character_skills),
]
