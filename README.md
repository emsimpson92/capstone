# Proposal for simple RPG system
My goal is to create a simple system that should be easyish to implement in software and that can be expanded with additions later.

## Characters
Characters, NPCs and Monsters are treated the same other than whether they are controlled by AI or human player.
I think we should start out with 3 attributes (+ health/hitpoints) to start but this could easily be expanded at any time. I propose starting with the following:
1. Intelligence: for magic/spells
2. Strength: for physical weapons, swords, maces, bows, etc
3. Dexterity: dodging, save throws, possibly can combine with skills/tasks that require the other 2

## Items
3 types of items:
1. Equipable: 
	* Weapons: swords and such but also spell books which is where I imaging the magic system coming in
	* Armor and pendants or other items that give buffs or make you harder to hit
2. Consumable: Potions, herbs, etc that are limited uses that give buffs and possibly other advantages
3. Just plain items: This would include coins/money, quest items, things that don’t actually effect the the stats or skill available to a character

This is all pretty basic and generic, so I will give my only possibly interesting idea:
## All available skills and actions come from the items
Examples:
* if you have a sword you have “slash” and “parry” available to you
* if you have a mace you have “bash” and “break armor” available
* if you have a spell book you can cast the particular spells in that book
* if you have a trap, you can lay it
There are probably tons of things we can do with items like this, each items has a list of actions which can take into account attributes and buffs/debuffs of the character wielding it as well as the character it’s being targeted on.  Maybe you can even target ally’s to give them buffs. This would also make it easy to create interesting monsters that had varying abilities. Shops and traders would be super simple to implement as well.

## Wiggle room
I feel this would give us a lot of wiggle room to implement interesting RPG elements. I think we can implement it and have plenty of time to also work on the social and geolocation features of the game.