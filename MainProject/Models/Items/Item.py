from django.db import models

class Item(models.Model):
	#ORM Fields
	itemType = models.IntegerField()
	name = models.CharField(max_length = 80)
	descripton = models.TextField()
	isSellable = models.BooleanField()
	equipType = models.IntegerField()
	HP = models.IntegerField()
	armor = models.IntegerField()
	strength = models.IntegerField()
	dexterity = models.IntegerField()
	wisdom = models.IntegerField()
	numberOfUses = models.IntegerField()

    def __init__(self, name, description, isSellable):
        self.name = name
        self.description = description
        self.isSellable = isSellable
