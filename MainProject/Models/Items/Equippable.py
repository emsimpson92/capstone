import Item
import EquipType

class Equippable(Item):
    def __init__(self, name, description, isSellable, EquipType, armor, HP, strength, wisdom, dexterity): 
        super().__init__(name, description, isSellable)
        self.EquipType = EquipType
        self.armor = armor
        self.HP = HP
        self.str = strength
        self.wis = wisdom
        self.dex = dexterity
