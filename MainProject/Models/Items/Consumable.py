import Item

class Consumable(Item):
    def __init__(self, name, description, isSellable, numberOfUses):
        super().__init__(name, description, isSellable)
        self.numberOfUses = numberOfUses

    def __del__(self):
        print(self.name, " charges used up. Destructing...")

    def Consume():
        if numberOfUses == 1:
            #effect
            del self
        else:
            numberOfUses -= 1
            #effect
