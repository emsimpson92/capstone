from django.db import models

class Player(models.Model):
	#ORM Fields
	name = models.CharField(max_length = 30)
	HP = models.IntegerField()
	armor = models.IntegerField()
	strength = models.IntegerField()
	dexterity = models.IntegerField()
	wisdom = models.IntegerField()
	level = models.IntegerField()

    def __init__(self, name):
        #self.playerID = xxx
        super().__init__(name)
