from django.db import models

class NPC(models.Model):
	#ORM Fields
	isFriendly = models.BooleanField()
	name = models.CharField(max_length = 30)
	HP = models.IntegerField()
	armor = models.IntegerField()
	strength = models.IntegerField()
	dexterity = models.IntegerField()
	wisdom = models.IntegerField()
	level = models.IntegerField()

    def __init__(self, name, isFriendly):
        self.isFriendly = isFriendly
        super().__init__(name)
