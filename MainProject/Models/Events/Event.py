from django.db import models

class Event(models.Model):
	#ORM Fields
	latitude = models.DecimalField()
	longitude = models.DecimalField()
	eventType = models.IntegerField()
	name = models.CharField(max_length = 80)
	description = models.TextField()
	reward = models.IntegerField()

    def __init__(self, lat, lon, description):
        #self.ID = xxx
        self.latitude = lat
        self.longitude = lon
        self.description = description
