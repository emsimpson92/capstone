import Event

class QuestEvent(Event):
    def __init__(self, lat, long, description):
        super().__init__(lat, long, description)
