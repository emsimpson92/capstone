import Event

class PVPEvent(Event):
    def __init__(self, lat, long, description, reward):
        super().__init__(lat, long, description)
        #reward will most likely be an item
        self.reward = reward
