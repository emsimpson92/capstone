import Event

class EncounterEvent(Event):
    def __init__(self, lat, long, description, enemies):
        super().__init__(lat, long, description)
        #enemies will be a list of NPCs in the encounter
        this.enemies = enemies
